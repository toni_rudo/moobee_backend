const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//=============================
// Schema
//=============================
const watchlistSchema = new Schema({
  userId: Schema.Types.ObjectId,
  films: [{
    filmId: {
      type: Number,
      unique: true,
    },
    hasLiked: {
      type: Boolean,
      default: null
    },
    isFavorite: {
      type: Boolean,
      default: null
    }
  }]
});

module.exports = mongoose.model('Watchlist', watchlistSchema);