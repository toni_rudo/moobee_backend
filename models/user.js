const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//=============================
// Schema
//=============================
const userSchema = new Schema({
  userName: {
    type: String,
    required: true,
    unique: true
  },
  firstName: {
    type: String,
  },
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String,
    required: true,
  },
  profilePic: {
    type: String,
  },
  bio: {
    type: String,
  },
  headerPic: {
    type: String,
  }
});

userSchema.methods.toJSON = function () {
  let obj = this.toObject();
  delete obj.password;
  delete obj.__v;
  return obj;
}

module.exports = mongoose.model('User', userSchema);