const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//=============================
// Schema
//=============================
const followedSchema = new Schema({
  userId: Schema.Types.ObjectId,
  follows: [
    Number
  ]
});

module.exports = mongoose.model('Followed', followedSchema);