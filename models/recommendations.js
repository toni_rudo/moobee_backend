const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//=============================
// Schema
//=============================
const recommendationsSchema = new Schema({
  userId: Schema.Types.ObjectId,
  recommendations: [{
    filmId: Number,
    userId: Number,
    isSeen: Boolean,
  }]
});

module.exports = mongoose.model('Recommendations', recommendationsSchema);