const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//=============================
// Schema
//=============================
const listSchema = new Schema({
  userId: Schema.Types.ObjectId,
  name: String,
  films: [
    Number
  ]
});

module.exports = mongoose.model('Lists', listSchema);