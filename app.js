var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var meRouter = require('./routes/me');
var searchRouter = require('./routes/search');
var watchlistRouter = require('./routes/watchlist');
var usersRouter = require('./routes/users');
const passport = require('passport');
const cloudinary = require('cloudinary');

var app = express();
require('./config/passport');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_SECRET
});

app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize())

app.use('/', indexRouter);
app.use('/me', meRouter);
app.use('/search', searchRouter);
app.use('/watchlist', watchlistRouter);
app.use('/users', usersRouter);

module.exports = app;