const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwtSecret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');

/* LOGIN. */
router.post('/login', (req, res, next) => {
  return passport.authenticate('login', {
    session: false
  }, function (err, user, info) {
    console.log(err, user, info)
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.status(info.code).json({
        error: true,
        data: info.message
      })
    } else {
      const token = jwt.sign({
        id: user.userName
      }, jwtSecret);

      return res.json({
        error: false,
        data: {
          user: User,
          token
        }
      })
    }
  })(req, res, next)
});

/* REGISTER user. */
router.post('/register', (req, res, next) => {
  return passport.authenticate('register', {
    session: false
  }, (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      console.log(info)
      return res.status(info.code).json({
        error: true,
        data: info.message
      })
    } else {
      req.logIn(user, err => {
        if (err) {
          return next(err);
        }

        User.findOneAndUpdate({
          userName: user.userName
        }, {
          firstName: req.body.firstName,
          email: req.body.email
        }, {
          new: true
        }, (err, User) => {
          if (err) {
            return next(err);
          }

          const token = jwt.sign({
            id: User.userName
          }, jwtSecret);

          return res.json({
            error: false,
            data: {
              user: User,
              token
            }
          })
        })
      })
    }
  })(req, res, next)
});

router.get('/validate', (req, res, next) => {
  const conditions = req.query;

  User.findOne(conditions, (err, User) => {
    if (err) {
      next(err)
    }

    const field = Object.keys(conditions)[0];

    if (User) {
      return res.status(400).json({
        error: true,
        data: `Ya existe ese ${field}`
      })
    } else {
      return res.json({
        error: false,
        data: `No existe ese ${field}`
      })
    }
  })
})
module.exports = router;