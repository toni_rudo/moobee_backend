const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Watchlist = require('../models/watchlist');
const passport = require('passport');

/* MARK: WATCHLIST ENDPOINTS */
/* GET FILM ON WATCHLIST */
router.get('/:filmId', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    const {
        filmId
    } = req.params;

    Watchlist.findOne({
        userId: req.user.id,
        'films.filmId': filmId
    }, (err, watchlist) => {
        if (err) {
            return next(err);
        }

        if (!watchlist) {
            return res.status(404).json({
                error: true,
                data: 'Film not found on users watchlist!'
            })
        } else {
            return res.json({
                error: false,
                data: watchlist.films.filter(value => value.filmId = filmId)
            })
        }
    })
})

/* ADD FILM TO WATCHLIST */
router.post('/add', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    const {
        filmId,
        watchDate
    } = req.body;

    Watchlist.findOne({
        userId: req.user.id
    }, (err, watchlist) => {
        if (err) {
            return next(err);
        }

        if (!watchlist) {
            watchlist = new Watchlist({
                userId: req.user.id,
                films: [{
                    filmId,
                    watchDate
                }]
            })

            watchlist.save();

            return res.json({
                error: false,
                data: watchlist
            })
        } else {
            Watchlist.findOneAndUpdate({
                    _id: watchlist.id
                }, {
                    $push: {
                        films: {
                            filmId,
                            watchDate,
                        }
                    }
                }, {
                    new: true
                },
                (err, watchlist) => {
                    if (err) {
                        return next(err);
                    }

                    return res.json({
                        error: false,
                        data: watchlist
                    });
                })
        }
    })
});

/* UPDATE FILM ON WATCHLIST */
router.put('/:filmId', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    const {
        filmId
    } = req.params;

    let newValues = {};

    Object.keys(req.body).map(value => {
        newValues[`films.$.${value}`] = req.body[value];
    });

    Watchlist.findOneAndUpdate({
        userId: req.user.id,
        'films.filmId': filmId
    }, {
        $set: newValues
    }, {
        new: true
    }, (err, watchlist) => {
        if (err) {
            return next(err);
        }

        if (!watchlist) {
            return res.status(404).json({
                error: true,
                data: 'Film not found on users watchlist!'
            })
        } else {
            return res.json({
                error: false,
                data: watchlist
            })
        }
    })
})

module.exports = router;