const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Watchlist = require('../models/watchlist');
const passport = require('passport');
const multer = require('multer');
const upload = require('../config/multer')
const cloudinary = require('cloudinary');
const fs = require('fs');

/* GET users listing. */
router.get('/', passport.authenticate('jwt', {
  session: false
}), (req, res, next) => {
  return res.json({
    error: false,
    data: req.user
  })
});

/* UPDATE user data */
router.put('/', passport.authenticate('jwt', {
  session: false
}), (req, res, next) => {
  User.findOneAndUpdate({
    _id: req.user.id
  }, req.body, (err, user) => {
    if (err) {
      return res.json({
        error: true,
        data: err
      })
    } else {
      return res.json({
        error: false,
        data: user
      })
    }
  })
})

/* UPLOAD PROFILE PIC */
router.post('/profile-pic', passport.authenticate('jwt', {
  session: false
}), (req, res, next) => {
  upload(req, res, (err) => {
    console.log(req)
    if (err) {
      return next(err);
    }

    const path = req.file.path;

    cloudinary.v2.uploader.upload(path, {
      public_id: req.user.id
    }, (err, avatar) => {
      if (err) {
        next(err);
      }

      User.findOneAndUpdate({
        _id: req.user.id
      }, {
        profilePic: avatar.secure_url
      }, {
        new: true
      }, (err, user) => {
        if (err) {
          next(err);
        }

        fs.unlink(path, (err) => {
          if (err) {
            next(err);
          }

          return res.json({
            error: false,
            data: user
          });
        });
      })


    })
  })
})

module.exports = router;