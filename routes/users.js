const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Watchlist = require('../models/watchlist');
const passport = require('passport');

router.get('/:userId', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    User.findById(req.params.userId, (err, User) => {
        if (err) {
            next(err)
        }

        console.log(User)
        if (!User) {
            return res.status(404).json({
                error: true,
                data: 'Usuario no encontrado'
            })
        } else {
            return res.json({
                error: false,
                data: User
            })
        }
    })
})

module.exports = router;