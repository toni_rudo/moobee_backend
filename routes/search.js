const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');

router.get('/users', (req, res, next) => {

    const userName = req.param('userName');

    const regexp = new RegExp(userName, 'i');

    console.log(regexp)
    User.find({
        userName: {
            $regex: regexp
        }
    }, (err, users) => {
        if (err) {
            return next(err);
        }

        return res.json({
            error: false,
            data: users
        })
    })
});

module.exports = router;