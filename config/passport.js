const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/user');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use('register', new LocalStrategy({
        usernameField: 'userName',
        passwordField: 'password',
        session: false
    },
    function (userName, password, cb) {
        return User.findOne({
                userName
            })
            .then(user => {
                if (!user) {
                    bcrypt.hash(password, 12, (err, hash) => {
                        if (err) {
                            return cb(null, false, {
                                code: 500,
                                message: 'Error al encriptar la contraseña'
                            })
                        }

                        const user = new User({
                            userName,
                            password: hash
                        });

                        user.save()
                            .then(user => {
                                return cb(null, user);
                            })
                            .catch(err => {
                                return cb(null, false, {
                                    code: 500,
                                    message: err
                                })
                            })
                    })
                } else {
                    return cb(null, false, {
                        code: 409,
                        message: 'El nombre de usuario ya existe'
                    });
                }
            })
            .catch(err => cb(err, false));
    }
));

passport.use('login', new LocalStrategy({
        usernameField: 'userName',
        passwordField: 'password',
        session: false
    },
    function (userName, password, cb) {
        return User.findOne({
                userName
            })
            .then(user => {
                if (!user) {
                    return cb(null, false, {
                        message: 'No se ha encontrado un usuario con ese nombre se usuario',
                        code: 404
                    });
                }
                bcrypt.compare(password, user.password)
                    .then(response => {
                        if (response === false) {
                            return cb(null, false, {
                                message: 'La contraseña no es correcta',
                                code: 400
                            })
                        }

                        return cb(null, user)
                    })
            })
            .catch(err => cb(err));
    }
));

const options = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: 'c0r1l@nd14',
}

passport.use('jwt', new JWTStrategy(options, (jwt_payload, cb) => {
    User.findOne({
            userName: jwt_payload.id
        })
        .then(user => {
            if (!user) {
                return cb(null, false, {
                    message: 'No se ha encontrado un usuario con ese nombre se usuario'
                });
            }

            return cb(null, user)
        })
        .catch(err => cb(err))
}));